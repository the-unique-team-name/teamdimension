/*:
 *
 * @plugindesc Add telemetry logging support for GAME27748 - Game Design 4: Data and Design
 * @author Douglas Gregory
 *
 * @param Host URL
 * @desc Address of the server to send data to.
 * Default: https://dd-telemetry.herokuapp.com/
 * @default https://dd-telemetry.herokuapp.com/
 * 
 * @param User Name
 * @desc Used to select your personal/team database table.
 * Default: Your team name or SLATE user name
 * @default Your team name or SLATE user name
 *
 * @param Secret
 * @desc Passphrase used to avoid accidental writes to the wrong table.
 * Default: Emailed to your group, or use student number
 * @default Emailed to your group, or use student number
 * 
 * @param Version
 * @desc Text used to identify each modified version of your game.
 * Default: 1.0
 * @default 1.0
 * 
 * @param Starting Section
 * @desc Name of the initial section of your game when starting up.
 * Default: Start
 * @default Start
 * 
 * @param Log to Server
 * @desc Should we log to the database (true), or just to the console (false)?
 * OFF - false     ON - true
 * Default: OFF
 * @default false
 * 
 * @help
 * Plugin Commands:
 *   TelemetrySection SectionName         # Set default section
 *                                          and log a SectionChange
 * 
 *   
 *   TelemetryLog Section EventType Data  # Log a custom event
 * 
 *        Section:   Name of the source (eg. map/mode, feature)
 *                   Type - to use the default section set earlier
 * 
 *        EventType: Name of the event
 * 
 *        Data:      Leave blank for no data
 *                   eg. TelemetryLog - GameStart
 * 
 *                   Can be a single number or text (no spaces)
 *                   eg. TelemetryLog - TalkTo George
 *                       TelemetryLog - TakeDamage 10                 
 * 
 *                   Can also be a sequence of pairs,
 *                       FieldName Value
 *                   eg. TelemetryLog - TalkTo Name George Mood Happy
 * 
 *                   Data values can also be read from variables.
 *                   eg. TelemetryLog - Move x v[1] y v[2]
 * 
 * 
 *   TelemetryIndex VariableSlot          # Put the session index
 *                                          into the numbered variable
 * 
 *                   eg. TelemetryIndex 3   sets variable 3 to contain
 *                                          the index of the current
 *                                          telemetry session
 */

Telemetry = {};

(function () {
    var parameters = PluginManager.parameters('Telemetry');
    var hostURL = String(parameters["Host URL"]);
    var serverEnabled = JSON.parse(parameters["Log to Server"]);
    var defaultSection = String(parameters["Starting Section"]);
    var logQueue = [];
    var networkInUse = false;
    var messageCount = 0;
    var sessionKey = "";
    var sessionIndex = -42;

    function httpGetAsync(url, success, error) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                if (request.status == 200)
                    success(request.response);
                else
                    error(request.response);
            }
                
        }
        request.open("GET", url, true); // true for asynchronous 
        request.send(null);
    }

    function httpPostJsonAsync(url, jsonString, success, error) {
        var request = new XMLHttpRequest();
        request.open("POST", url, true);

        request.setRequestHeader("Content-Type", "application/json");
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                if (request.status == 200)
                    success(request.response);
                else
                    error(request.responseText);
            }
        }
        
        request.send(jsonString);
    }

    function wakeAttempt(response) {
        networkInUse = true;
        console.log(`Attempting to connect to telemetry server, attempt #${++messageCount}...`);
        httpGetAsync(hostURL + "awake", onWakeSuccess, wakeAttempt);
    }

    function onWakeSuccess(response) {
        var userName = String(parameters["User Name"])
        console.log(`Telemetry server is awake. Authenticating as ${userName}`);

        httpPostJsonAsync(
            hostURL + "connect",
            JSON.stringify({
                userName: userName,
                secret: String(parameters["Secret"]),
                version: String(parameters["Version"]),
                section: defaultSection
            }),
            onAuthSuccess,
            (response) => { console.log("Telemetry authentication error: ", response) }
        );
    }

    function onAuthSuccess(response) {
        var body = JSON.parse(response);        
        sessionKey = body.sessionKey;
        sessionIndex = body.sessionIndex;
        console.log("Connected to telemetry server: ", body.message);

        if (logQueue.length > 0) {
            logLoop();
        } else {
            networkInUse = false;
        }
    }    

    function decodeVariables(text) {
        var extractVariable = function () {
            return $gameVariables.value(parseInt(arguments[1]));
        }.bind(this);

        var modified = text;
        do {
            text = modified;
            var modified = text.replace(/V\[(\d+)\]/gi, function () {
                return $gameVariables.value(parseInt(arguments[1]));
            }.bind(this));
        } while (modified !== text)

        return modified;
    }

    function parseArgument(text) {
        text = decodeVariables(text);
        if (!isNaN(text)) {
            var asNumber = parseFloat(text);
            if (!isNaN(asNumber)) return asNumber;
        }
        return text;
    }

    Telemetry.getSessionIndex = function() {
        return sessionIndex;
    }

    Telemetry.getDefaultSection = function () {
        return defaultSection;
    }

    Telemetry.setSection = function (section) {
        if (section === defaultSection) return;
        Telemetry.log(defaultSection, "ChangeSection", section);
        defaultSection = section;
    }

    Telemetry.log = function (section, eventType, data) {
        if (section === undefined || section === null || section === "")
            section = defaultSection;
        
        if (data === undefined || data === null)
            data = {};
        
        var message = {
            section: section,
            eventType: eventType,
            timecode: Date.now(),
            data: data,
        }
        logQueue.push(message);        
        if (!networkInUse) {
            networkInUse = true;
            logLoop();
        }
    }

    function onLogResponse(response) {
        if (logQueue.length > 0) {
            setTimeout(logLoop, 100);
        } else {
            networkInUse = false;
        }
    }

    function logLoop() {
        var message = logQueue.shift();
        message.sessionKey = sessionKey;
        message.sequence = messageCount++;
        var json = JSON.stringify(message);
        console.log("Logging telemetry:", json);
        if (serverEnabled) {
            httpPostJsonAsync(hostURL + "log", json, onLogResponse, onLogResponse);
        } else {
            onLogResponse();
        }
    }

    var Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;

    Game_Interpreter.prototype.pluginCommand = function (command, args) {
        Game_Interpreter_pluginCommand.call(this, command, args);

        switch (command.toLowerCase()) {
            case "telemetrysection":
                if (args[0] == undefined) {
                    console.log("Error - TelemetrySection command needs a name for the new section.");
                    break;
                }
                var section = decodeVariables(args[0]);
                Telemetry.setSection(section);
                break;
            
            case "telemetryindex":
                if (args[0] == undefined) {
                    console.log("Error - TelemetryIndex command needs a variable number to put the index into");
                    break;
                }
                var destination = args[0];
                if ((destination.startsWith("v[") || destination.startsWith("V["))&&destination.endsWith("]")) {
                    destination = parseInt(destination.slice(2, -1));
                } else {
                    destination = parseInt(destination);
                }
                if(isNaN(destination)) {
                    console.log("Error - TelemetryIndex command needs a variable number to put the index into");
                    break;
                }
                $gameVariables.setValue(destination, sessionIndex);
                break;
            
            case "telemetrylog":
                if (args.length < 2) {
                    console.log("Error - TelemetryLog needs at least two arguments: a section (or -) and an event type. Arguemnts:", args);
                    break;
                }
                var section = decodeVariables(args[0]);
                if (section === '-') section = defaultSection;

                var eventType = decodeVariables(args[1]);

                var data = {};

                if (args.length === 3) {
                    data = parseArgument(args[2]);
                } else if (args.length > 3) {
                    if (args.length % 2 != 0) {
                        console.log("Error - TelemetryLog arguments should occur in pairs: field name, value. Arguemnts:", args);
                        break;
                    }
                    for (var i = 2; i < args.length; i += 2) {
                        data[decodeVariables(args[i])] = parseArgument(args[i+1]);
                    }
                }
                Telemetry.log(section, eventType, data);
                break;
        }
    };

    if (serverEnabled) {
        wakeAttempt();
    } else {
        console.log("Telemetry session started - local logging only.");
    }
})();